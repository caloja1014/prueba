import { Component, OnInit } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AppLauncher, AppLauncherOptions } from '@ionic-native/app-launcher/ngx';
import { Platform } from '@ionic/angular';
import { Calendar } from '@ionic-native/calendar/ngx';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  formDel!: FormGroup;
  constructor(
    private localNotifications: LocalNotifications,
    private appLauncher: AppLauncher,
    private platform: Platform,
    private calendar: Calendar
  ) {
    const options: AppLauncherOptions = {
    };
    // const startDate = new Date(2021, 9, 14);
    // const endDate = new Date(2021, 9, 15);
    // this.calendar.createEvent('"receta"', '"casa"', '"tomar"', startDate, endDate).then(
    //   res=>{
    //     console.log('eventooooooooooooo');
    //     console.log(res);
    //   }
    // );
    // this.calendar.listCalendars().then(
    //   res=>{
    //     console.log('calendarios');
    //     console.log(res);
    //   }
    // );
    // this.calendar.listEventsInRange(startDate, endDate).then(
    //   res=>{
    //     console.log('eventos');
    //     console.log(res);
    //   }
    // );
    // this.calendar.listEventsInRange(startDate, endDate).then(
    //   res=>{
    //     console.log('eventos');
    //     console.log(res);
    //   }
    // );
    // if (this.platform.is('ios')) {
    //   options.uri = 'https://io.ionic.starter/';
    // } else {
    //   options.packageName = 'io.ionic.starter';
    // }
    // this.appLauncher.canLaunch(options)
    //   .then((canLaunch: boolean) => console.log('App is available'))
    //   .catch((error: any) => console.log('App is not available'));
    // console.log('"TIENE PERMISOS"');

    // this.localNotifications.schedule({
    //   text: 'Delayed ILocalNotification',
    //   trigger: { at: new Date(new Date().getTime() + 20000) },
    //   led: 'FF0000',
    //   sound: null
    // });
    // this.localNotifications.on('trigger').subscribe(
    //   () => {
    //     console.log('SE HA LANZSADOOO');
    //     this.appLauncher.launch(options);
    //   }
    // );
  }
  ngOnInit() {
    this.formDel = new FormGroup({
      id: new FormControl('', [Validators.required]),
    });
  }
  addEvente() {
    console.log('"AÑADIR EVENTO"');
    const startDate = new Date(2021, 8, 17);
    const endDate = new Date(2021, 8, 18);
    this.calendar.createEvent('"receta4"', '"casa"', '"tomar"', startDate, endDate).then(
      res => {
        console.log('eventooooooooooooo');
        console.log(res);
      },
      err => {
        console.log('error');
        console.log(err);
      }
    );
  }
  addOptions() {
    const calOptions = this.calendar.getCalendarOptions();
    const startDate = new Date(2021, 8, 17);
    const endDate = new Date(2021, 8, 18);
    calOptions.calendarId = 11;
    calOptions.recurrence = 'monthly';
    calOptions.calendarName = 'MyCalendar';
    calOptions.calendarId = 8;
    this.calendar.createEventWithOptions('tomarReceta mensual', 'casa', 'F', startDate, endDate, calOptions).then(
      res => {
        console.log('Add success');
        console.log(res);
        console.log(calOptions);
      }
    ).catch(
      err => {
        console.log('Fallo');
        console.log(err);
      }
    );
  }
  openCalendar() {
    this.calendar.openCalendar(new Date()).then(
      (msg) => { console.log(msg); },
      (err) => { console.log(err); }
    );
  }
  addCalendar(){
    this.calendar.createCalendar('MyCalendar').then(
      res=>{
        console.log('Add success');
        console.log(res);
      }
    );
  }
  showCalendars() {
    this.calendar.listCalendars().then(
      res => {
        console.log('calendarios');
        console.log(res);
      }
    );
  }
  showEvents() {
    const startDate = new Date(2021, 8, 14);
    const endDate = new Date(2021, 9, 15);
    this.calendar.listEventsInRange(startDate, endDate).then(
      res => {
        console.log('eventos 09');
        console.log(res);
      }
    );

  }
  eliminarEvento() {
    const startDate = new Date(2021, 8, 14);
    this.calendar.deleteEventById(this.formDel.value.id, startDate).then(
      res => {
        console.log(this.formDel.value.id);
        console.log('all is cool');
        this.formDel.reset();
      }
    );
  }
  permisions() {
    this.calendar.hasReadWritePermission().then(
      res => {
        console.log('permisos');
        console.log(res);
      }
    );
    this.calendar.deleteCalendar('MyCalendar').then(
      res => {
        console.log('se elimino correctamente');
        console.log(res);
      }
    ).catch(
      err => {
        console.log('F no se pudo eliminar');
        console.log(err);
      }
    );
  }
}
